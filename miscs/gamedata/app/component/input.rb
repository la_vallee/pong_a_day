class Input

  def initialize(args)
    @args = args
  end

  def args
    @args
  end

  def check_keyboard
    if args.inputs.keyboard.key_down.escape
      args.gtk.request_quit
    end
    input
  end

  def input
    input_left_paddle
    input_right_paddle
  end

  def input_left_paddle
    if args.inputs.controller_one.key_down.down  || args.inputs.keyboard.key_down.s
      args.state.left_paddle.y -= 40
    elsif args.inputs.controller_one.key_down.up || args.inputs.keyboard.key_down.w
      args.state.left_paddle.y += 40
    end
  end

  def input_right_paddle
    if args.inputs.controller_two.key_down.down  || args.inputs.keyboard.key_down.l
      args.state.right_paddle.y -= 40
    elsif args.inputs.controller_two.key_down.up || args.inputs.keyboard.key_down.o
      args.state.right_paddle.y += 40
    end
  end
end
