class Gui
  def initialize(args)
    @args = args
  end

  def args
    @args
  end

  def tick
    render_instructions
  end

  def render_instructions
    args.outputs.labels << [320, 30, "W and S keys to move left paddle.",  0, 1]
    args.outputs.labels << [920, 30, "O and L keys to move right paddle.", 0, 1]
  end
end
