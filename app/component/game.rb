class Game

  def initialize(args)
    @args  = args
    @gui   = Gui.new  args
    @ball  = Ball.new args
    @calc  = Calc.new args
    @input = Input.new args
  end

  def args
    @args
  end

  def tick
    render
    render_countdown
    render_paddles
    @gui.tick
    @ball.tick
    @calc.process
    @input.check_keyboard
  end

  def render
    args.outputs.lines  << [640, 0, 640, 720]
  end

  def render_countdown
    return unless args.state.ball.debounce > 0
    args.outputs.labels << [640, 360, "%.2f" % args.state.ball.debounce.fdiv(60), 10, 1]
  end

  def render_paddles
    args.outputs.solids << solid_left_paddle
    args.outputs.solids << solid_right_paddle
  end

  def solid_left_paddle
    centered_rect_vertically 0, args.state.left_paddle.y, args.state.paddle.w, args.state.paddle.h
  end

  def solid_right_paddle
    centered_rect_vertically 1280 - args.state.paddle.w, args.state.right_paddle.y, args.state.paddle.w, args.state.paddle.h
  end

  def centered_rect_vertically x, y, w, h
    [x, y - h / 2, w, h]
  end

end
