class Calc

  def initialize(args)
    @args = args
  end

  def args
    @args
  end

  def state
    @args.state
  end

  def ball
    state.ball
  end

  def process
    args.state.ball.debounce -= 1 and return if args.state.ball.debounce > 0
    move_ball
    collision_with_left_paddle
    collision_with_right_paddle
    collision_with_walls
  end

  def move_ball
    ball.x += ball.dx
    ball.y += ball.dy
  end

  def collision_with_left_paddle
    #if ball.x < 300
    #  ball.dx *= -1
    #end
    #return
    if solid_left_paddle.intersect_rect? solid_ball
      ball.dx *= -1
    elsif ball.x < 0
      args.state.right_paddle.score += 1
      reset_round
    end
  end

  def collision_with_right_paddle
    #if ball.x > 800
    #  ball.dx *= -1
    #end
    #return
    if solid_right_paddle.intersect_rect? solid_ball
      ball.dx *= -1
    elsif ball.x > 1280
      args.state.left_paddle.score += 1
      reset_round
    end
  end

  def collision_with_walls
    if ball.y + ball.size_half > 720
      ball.y = 720 - ball.size_half
      ball.dy *= -1
    elsif ball.y - ball.size_half < 0
      ball.y = ball.size_half
      ball.dy *= -1
    end
  end

  def solid_ball
    centered_rect args.state.ball.x, args.state.ball.y, args.state.ball.size, args.state.ball.size
  end

  def reset_round
    ball.x        = 640
    ball.y        = 360
    ball.dx       = 5.randomize(:sign)
    ball.dy       = 5.randomize(:sign)
    ball.debounce = 3 * 60
  end

  def solid_left_paddle
    centered_rect_vertically 0, args.state.left_paddle.y, args.state.paddle.w, args.state.paddle.h
  end

  def solid_right_paddle
    centered_rect_vertically 1280 - args.state.paddle.w, args.state.right_paddle.y, args.state.paddle.w, args.state.paddle.h
  end

  def centered_rect x, y, w, h
    [x - w / 2, y - h / 2, w, h]
  end

  def centered_rect_vertically x, y, w, h
    [x, y - h / 2, w, h]
  end
end
