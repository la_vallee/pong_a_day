class Ball

  def initialize(args)
    @args = args
    init
  end

  def args
    @args
  end

  def state
    @args.state
  end

  def x
    ball.x - 32 / 2
  end

  def y
    ball.y - 32 / 2
  end

  def ball
    state.ball
  end

  def tick
    render
  end

  def render
    args.outputs.solids << solid_ball
    args.outputs.sprites << [x, y, 32, 32, 'sprites/blue_mushroom.png']
  end

  def solid_ball
    centered_rect ball.x, ball.y, ball.size, ball.size
  end

  private

  def centered_rect x, y, w, h
    [x - w / 2, y - h / 2, w, h]
  end

  def init
    ball.debounce       ||= 3 * 60
    ball.size           ||= 10
    ball.size_half      ||= ball.size / 2
    ball.x              ||= 640
    ball.y              ||= 360
    ball.dx             ||= 5.randomize(:sign)
    ball.dy             ||= 5.randomize(:sign)
    state.left_paddle.y       ||= 360
    state.right_paddle.y      ||= 360
    state.paddle.h            ||= 120
    state.paddle.w            ||= 10
    state.left_paddle.score   ||= 0
    state.right_paddle.score  ||= 0
  end

end
