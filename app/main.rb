require "app/component/input.rb"
require "app/component/gui.rb"
require "app/component/calc.rb"
require "app/component/ball.rb"
require "app/component/game.rb"

def tick args
  game ||= Game.new args
  game.tick
end
